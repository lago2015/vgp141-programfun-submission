﻿using UnityEngine;
using System.Collections;

public class FlyWeight : MonoBehaviour {

    void Start()
    {
        // Arbitrary extrinsic state 
        int extrinsicstate = 22;

        FlyweightFactory f = new FlyweightFactory();

        // Work with different flyweight instances 
        Flyweight fx = f.GetFlyweight("X");
        fx.Operation(--extrinsicstate);

        Flyweight fy = f.GetFlyweight("Y");
        fy.Operation(--extrinsicstate);

        Flyweight fz = f.GetFlyweight("Z");
        fz.Operation(--extrinsicstate);

        UnsharedConcreteFlyweight uf = new
          UnsharedConcreteFlyweight();

        uf.Operation(--extrinsicstate);

        // Wait for user 
        //Console.Read();
    }   
    

    // FlyweightFactory Class
    class FlyweightFactory
    {
        private Hashtable flyweights = new Hashtable();

        // Constructor 
        public FlyweightFactory()
        {
            flyweights.Add("X", new ConcreteFlyweight());
            flyweights.Add("Y", new ConcreteFlyweight());
            flyweights.Add("Z", new ConcreteFlyweight());
        }

        public Flyweight GetFlyweight(string key)
        {
            return ((Flyweight)flyweights[key]);
        }
    }

    // Flyweight Class
    abstract class Flyweight
    {
        public abstract void Operation(int extrinsicstate);
    }

    // ConcreteFlyweight 

    class ConcreteFlyweight : Flyweight
    {
        public override void Operation(int extrinsicstate)
        {
            //Console.WriteLine("ConcreteFlyweight: " + extrinsicstate);
            if(extrinsicstate==21)
            {
                Debug.Log("FlyweightX: " + extrinsicstate);
            }
            if (extrinsicstate == 20)
                Debug.Log("FlyweightY: " + extrinsicstate);
            if (extrinsicstate == 19)
                Debug.Log("FlyweightZ: " + extrinsicstate);
        }
    }

    // UnsharedConcreteFlyweight
    class UnsharedConcreteFlyweight : Flyweight
    {
        public override void Operation(int extrinsicstate)
        {
           // Console.WriteLine("UnsharedConcreteFlyweight: " +
             // extrinsicstate);
            Debug.Log("UnsharedConcreteFlyweight: " + extrinsicstate);
        }
    }
}
