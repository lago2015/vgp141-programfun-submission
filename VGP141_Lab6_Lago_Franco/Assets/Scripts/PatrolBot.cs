﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PatrolBot : MonoBehaviour
{
    public enum EnemyState { Patrol, Attack }
    public EnemyState curState;

    //EnemyPatrol
    public Transform[] wayPath;
    public float waySpeed = 3.0f;   //speed of patrol
    public int wayCount=0;
    public bool wayLoop = true;     //if waypoints loop
    public float wayLook = 10;
    public float wayPause = 0.0f;   //pause on each waypoint
    private float currentTime = 0.0f;
    private int currentPoint = 0;   //current array position
    private CharacterController character;  //AI must have a character controler attached

    //EnemyAttack
    public Transform Player;
    public Transform enemy;
    public Transform Bullet;
    public Transform gun { get; set; }
    public Transform bulletSpawnPoint { get; set; }
    public GameObject[] enemies;


    public int rotSpd = 180;
    public float elapsedTime = 5.0f;
    public float burstTime = 0;
    public float moveSpd = 3.0f;
    public float shootRate = 3.0f;
    public float burstRate = 1.0f;
    public bool fireNow = false;

    // Use this for initialization
    void Start()
    {
        character = GetComponent<CharacterController>();
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        gun = gameObject.transform.GetChild(0).transform;
        bulletSpawnPoint = gun.GetChild(0).transform;
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        //wayPath = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().node;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        elapsedTime += Time.deltaTime;

        switch (curState)
        {

            case EnemyState.Attack:
                Attack();
                break;
            case EnemyState.Patrol:
                if (currentPoint < wayPath.Length)
                {
                    EnemyPatrol();
                }
                else
                {
                    if (wayLoop)
                    {
                        currentPoint = 0;
                    }
                }
                break;
        }
        if (elapsedTime >= 4)
            elapsedTime = 0;
        if (burstTime >= 5)
            burstTime = 0;
    }

    void EnemyPatrol()
    {

        Vector3 currentPlace = wayPath[currentPoint].position;
        Vector3 direction = currentPlace - transform.position;

        //applying gravity
        if (character.isGrounded)
            direction.y = -0.1f;

        else
            direction.y += Physics.gravity.y;

        float step = waySpeed * Time.deltaTime;

        if (direction.magnitude < 0.5)
        {
            if (currentTime == 0)
            {
                currentTime = Time.time;
            }
            //loops back to 0 if wayLoop is true
            if ((Time.time - currentTime) >= wayPause)
            {
                currentPoint++;
                currentTime = 0;
            }
        }

        //Looking for waypoint
        else if (direction.magnitude > 0.5)
        {
            Quaternion rotation = Quaternion.LookRotation(currentPlace - transform.position);   //grabs location from gameobject to waypoint
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * wayLook);      //calculate rotation
            character.Move(direction.normalized * step);
        }
    }

    public void Attack()
    {
        transform.LookAt(Player);      //calculate rotation
        burstTime += Time.deltaTime;
        if (elapsedTime >= shootRate)
        {

            if (burstTime >= burstRate)
            {
                Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
                burstTime = 0;
            }
            elapsedTime = 0.0f;
        }
        
    }

    void OnTriggerStay(Collider col)
    {
        //Debug.Log("Hit");

        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Player");
            curState = EnemyState.Attack;
        }
        if(col.gameObject.tag=="Enemy")
        {
            EnemyState enemy = col.gameObject.GetComponent<PatrolBot>().curState;
            if (enemy == EnemyState.Attack)
            {
                curState = EnemyState.Attack;
            }
            //else if(enemy==EnemyState.Patrol)
            //{
            //    curState = EnemyState.Patrol;
            //}
        }
    }
    void OnTriggerExit(Collider cc)
    {
        if (cc.gameObject.tag == "Player")
        {
            curState = EnemyState.Patrol;
        }
    }
}