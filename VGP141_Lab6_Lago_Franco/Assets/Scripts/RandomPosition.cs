﻿using UnityEngine;
using System.Collections;

public class RandomPosition : MonoBehaviour {

    float posY;
    float posX;
    float posZ;
	// Use this for initialization
	void Start () 
    {
        posY = Random.Range(Random.Range(1, 20), Random.Range(1, 20));
        posX = Random.Range(Random.Range(1, 20), Random.Range(1, 20));
        posZ = Random.Range(Random.Range(1, 20), Random.Range(1, 20));
        transform.position = new Vector3(posX, 0, posZ);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
