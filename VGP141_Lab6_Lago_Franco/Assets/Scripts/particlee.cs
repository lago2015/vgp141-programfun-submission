﻿using UnityEngine;
using System.Collections;

public class particlee : SandboxSubclass
{
    public ParticleSystem particle;
    public AudioSource audio;
    public AudioClip[] sounds;
    public override void Start()
    {
        particle = gameObject.GetComponent<ParticleSystem>();
        particle.startColor = Color.blue;
        gameObject.transform.position = new Vector3(Random.Range(0, 10), Random.Range(0, 10), 0);

        audio = gameObject.GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            audio.clip = sounds[Random.Range(0, sounds.Length)];
            audio.Play();
        }

    }
}


