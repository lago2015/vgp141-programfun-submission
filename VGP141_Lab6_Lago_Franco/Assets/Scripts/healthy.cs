﻿using UnityEngine;
using System.Collections;

public class healthy : MonoBehaviour {

    int health = 3;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            health--;
            if (health < 0)
                Destroy(gameObject);
        }
    }
}
