﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

    public GameObject bullet;
    public Transform gun { get; set; }
    public Transform bulletSpawnPoint { get; set; }
	
    void Start()
    {
        gun = gameObject.transform.GetChild(0).transform;
        //bulletSpawnPoint = gun.gameObject.transform.GetChild(0).transform;
    }

	// Update is called once per frame
	void Update () 
    {
	    if(Input.GetKeyDown(KeyCode.Q))
        {
            Instantiate(bullet, gun.position, Quaternion.identity);
        }
	}
}
