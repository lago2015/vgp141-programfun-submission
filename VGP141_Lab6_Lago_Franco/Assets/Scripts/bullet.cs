﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

    public float moveSpeed = 5.0f;
    public float lifeSpan = 5.0f;
    public float damage = 1.0f;
    // Update is called once per frame


    void FixedUpdate()
    {
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
        //transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        lifeSpan -= Time.deltaTime;
        if (lifeSpan <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
